package com.derivatExpression;

import java.util.ArrayList;
import java.util.List;

import com.derivatExpression.ExpressionAdapter.EXPRESSION_SYMBOL;

 

 
 
public class Derivative {
	
	public static List<String> logDerv = new ArrayList<String>();
	private Expression exp;
	private Expression exp_action;
	
	/**
	  Just for testing the Derivative class
	 * @param args
	 */
	public static void main(String[] args){
		String exp_string = "a.(-b)*.a";
		 logDerv.clear();
	    ExpressionAdapter expAdapter = new ExpressionAdapter(exp_string);
 
		Expression expression  = expAdapter.getExpression();
 
		 
		 Expression exp_action = new  Expression();
		 exp_action.name_action = "a";
		 
		 Derivative derivative = new Derivative(expression, exp_action);
		 
		 Expression expressionDerivative  = derivative.getExpression();
		 
		for (String string :  logDerv) {
			System.out.println(string);
		}
		
		System.out.println(expressionDerivative.purgeExpression().toString());
	}
	

	/**
	  Build the derivative object with two parameters 
	 * @param exp : expression to derivative  
	 * @param exp_action : 
	 */
	public Derivative(Expression exp,Expression exp_action){
		 this.exp=exp;
		 this.exp_action=exp_action;
	 }
	 
	private static void LOG_ADD(String source,String action,String result){
		 logDerv.add(source+"/"+action+" --> "+result); 
	 }
	
	public Expression getExpression(){
		
		Expression exp_result;
		String source = exp.toString();
		
		if(exp.with_not && exp.with_star){
			if(exp.not_is_first){
 
				Expression exp_col = exp.clone();
				
				exp_col.with_not= false;
				exp_col.not_is_first= false;
				
				exp_result = this.deriv_star(exp_col, exp_action);
				exp_result.with_not= true;
				exp_result.not_is_first= true;
				
				LOG_ADD(source,exp_action.toString(),exp_result.toString());
				
				return exp_result;
			}else{
				 
				exp_result = new Expression();
				exp_result.exp_droite = exp;
				exp_result.operateur = EXPRESSION_SYMBOL.operator_concatenation;
				
				Expression exp_gauche = exp.clone();
				
				exp_gauche.with_star = false;
				exp_gauche.not_is_first= false;
 
				exp_result.exp_gauche = this.deriv_not(exp_gauche, exp_action);
				
				LOG_ADD(source,exp_action.toString(),exp_result.toString());
				return exp_result;
			}
		}
		
		
		if(exp.with_not){
			 
			 exp_result= deriv_not(exp,exp_action);
			 LOG_ADD(source,exp_action.toString(),exp_result.toString());
			 return exp_result;
		}
		if(exp.with_star){
		 
			exp_result= deriv_star(exp,exp_action);
			LOG_ADD(source,exp_action.toString(),exp_result.toString());
			 return exp_result;
		}
		
		 if(exp.name_action.equals("")){//expression
			 
			 if(exp.operateur ==  EXPRESSION_SYMBOL.operator_concatenation){
				 
				 exp_result= deriv_mult(exp,exp_action);
				 LOG_ADD(source,exp_action.toString(),exp_result.toString());
				 return exp_result;
			 }
			 
			 if(exp.operateur ==  EXPRESSION_SYMBOL.operator_union){
			 
				 exp_result= deriv_add(exp,exp_action);
				 LOG_ADD(source,exp_action.toString(),exp_result.toString());
				 return exp_result;
			 }
			 if(exp.operateur ==  EXPRESSION_SYMBOL.operator_nothing){
				 
				 exp_result= this.deriv_nothing(exp,exp_action);
				 LOG_ADD(source,exp_action.toString(),exp_result.toString());
				 return exp_result;
			 }
			 
		 } else{//action
		 
			 exp_result= deriv_constant(exp,exp_action);
			 LOG_ADD(source,exp_action.toString(),exp_result.toString());
			 return exp_result;
		 }
		 
		 LOG_ADD(source,exp_action.toString(),exp.toString());
		 
		return exp;
	}
	 
	private Expression deriv_nothing(Expression exp2, Expression exp_action2) {
		
		Expression return_exp =new Expression();
		
		Expression left_exp =new Expression();
		
		Expression tmp_exp = exp2.exp_gauche.clone();
		tmp_exp.with_star = false;
		
		left_exp.exp_gauche = new Derivative(tmp_exp,exp_action2).getExpression();  
		left_exp.operateur  = EXPRESSION_SYMBOL.operator_concatenation;
		left_exp.exp_droite = exp2;  
		
		return_exp.exp_gauche = left_exp;
		return_exp.operateur  = EXPRESSION_SYMBOL.operator_union;
		return_exp.exp_droite = new Derivative(exp2.exp_droite,exp_action2).getExpression();
		
		return return_exp;
	}


	private Expression deriv_star(Expression exp2, Expression exp_action2) {
 
  
		Expression return_exp =new Expression();
 
		Expression exp_col = exp2.clone();
		
		exp_col.with_star = false;
	 
		
		Derivative derv  = new Derivative(exp_col,exp_action2);
		return_exp.exp_gauche = derv.getExpression();
		return_exp.operateur = EXPRESSION_SYMBOL.operator_concatenation;
		
		exp2.with_star = true;
		return_exp.exp_droite = exp2;
 
		return return_exp;
	}

	private Expression deriv_not(Expression exp2, Expression exp_action2) {
		
		
		Expression return_exp =new Expression();
		Expression exp_col = new Expression();
		
		exp_col.exp_gauche = exp2.exp_gauche;
		exp_col.exp_droite = exp2.exp_droite;
		
		exp_col.operateur = exp2.operateur;
		
		exp_col.with_star = exp2.with_star;
		exp_col.with_not   = false;
		exp_col.not_is_first = exp2.not_is_first;
		
		exp_col.name_action=exp2.name_action;
		
		Derivative derv  = new Derivative(exp_col,exp_action2);
		
		return_exp = derv.getExpression();
		return_exp.with_not = true;
		
		return return_exp;
	}

	private Expression deriv_constant(Expression exp,Expression exp_action){
		
		if(exp.name_action.equals(exp_action.name_action)){
			Expression return_exp =new Expression();
			return_exp.name_action = "1";
			return return_exp;
		}else{
			Expression return_exp =new Expression();
			return_exp.name_action = "0";
			return return_exp;
		}
  
	}
	
	private Expression deriv_mult(Expression exp,Expression exp_action){
		 
		Expression return_exp =new Expression();
		Derivative derv  = new Derivative(exp.exp_gauche,exp_action);
		
		return_exp.exp_gauche = derv.getExpression();
		return_exp.exp_droite = exp.exp_droite ;
		return_exp.operateur = EXPRESSION_SYMBOL.operator_concatenation;
		
		 return return_exp;
	}
	
	private Expression deriv_add(Expression exp,Expression exp_action){
		 
		Expression return_exp =new Expression();
		Derivative derv_gauche  = new Derivative(exp.exp_gauche,exp_action);
		Derivative derv_droite  = new Derivative(exp.exp_droite,exp_action);
		
		return_exp.exp_gauche = derv_gauche.getExpression();
		return_exp.exp_droite = derv_droite.getExpression();
		return_exp.operateur = EXPRESSION_SYMBOL.operator_union;
		
		return return_exp;
	}
 
}
