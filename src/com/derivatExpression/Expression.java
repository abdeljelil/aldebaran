package com.derivatExpression;

import com.derivatExpression.ExpressionAdapter.EXPRESSION_SYMBOL;
 

/**
 * 
 * @author IMENE HADDED
 * Expression model
 *
 */
 
public	class Expression implements Cloneable{
	 
	public Expression exp_gauche = null;
	public Expression exp_droite = null;
	
	public int operateur = 0;
	
	public boolean with_star 	= false;
	public boolean with_not   	= false; 
	public boolean not_is_first	= false;
	
	public String name_action="";
	
	public String toString(){
		String result = "";
		
		 if(name_action.equals("")){
			 
			 result	+= "("+exp_gauche.toString()+
			 ExpressionAdapter.symbolToOperateur(operateur)+
			 exp_droite.toString()+")";
		 } else
			 result += name_action;
		 
		 if(with_not && with_star){
			 if(not_is_first)  result = "-("+result+"*)"; else result = "(-"+result+")*";
		 }else{
			 if(with_not)  result = "-"+result;
			 if(with_star) result += "*";
		 }
		 return result;
	}
	
	public boolean equals(Expression exp){
		
		return toString().equals(exp.toString());
	}
	public Expression purgeExpression(){
		return this.purgeExpression(this);
	}
	private Expression purgeExpression(Expression exp){
		 
		
		 //noeud
		if(!exp.name_action.equals("")) {
			String name = exp.name_action ;
			
			 if(exp.with_not && exp.with_star && exp.not_is_first && name.equals("1")) {
				 exp.name_action = "0";
				 exp.with_not =false; exp.with_star =false; exp.not_is_first=false;
				 return exp;
			 }
			 if(exp.with_not && exp.with_star && exp.not_is_first && name.equals("0")) {
				 exp.name_action = "1";
				 exp.with_not =false; exp.with_star =false; exp.not_is_first=false;
				 return exp;
			 }
			 if(exp.with_not && exp.with_star && !exp.not_is_first && name.equals("0")) {
				 exp.name_action = "1";
				 exp.with_not =false; exp.with_star =false; exp.not_is_first=false;
				 return exp;
			 }
			 if(exp.with_not && exp.with_star && !exp.not_is_first && name.equals("1")) {
				 exp.name_action = "0";
				 exp.with_not =false; exp.with_star =false; exp.not_is_first=false;
				 return exp;
			 }
			 if(exp.with_not   && name.equals("1")) {
				 exp.name_action = "0";
				 exp.with_not =false; exp.with_star =false; exp.not_is_first=false;
				 return exp;
			 }
			 if(exp.with_not   && name.equals("0")) {
				 exp.name_action = "1";
				 exp.with_not =false; exp.with_star =false; exp.not_is_first=false;
				 return exp;
			 }
			 if(exp.with_star   && name.equals("0")) {
				 exp.name_action = "0";
				 exp.with_not =false; exp.with_star =false; exp.not_is_first=false;
				 return exp;
			 }
			 if(exp.with_star   && name.equals("1")) {
				 exp.name_action = "1";
				 exp.with_not =false; exp.with_star =false; exp.not_is_first=false;
				 return exp;
			 }
			 
			 
			return exp;
		}
		 //gauche est une expression
		 if(exp.exp_gauche != null && exp.exp_gauche.name_action.equals(""))  
			 exp.exp_gauche =  purgeExpression(exp.exp_gauche);
		 
		 //droite est une expression
		 if(exp.exp_droite != null &&  exp.exp_droite.name_action.equals(""))  
			 exp.exp_droite =  purgeExpression(exp.exp_droite);
		 
	 
		 
		 if(exp.operateur ==  EXPRESSION_SYMBOL.operator_concatenation){
			 //verification de gauche 
				 if(exp.exp_gauche.name_action.equals("1")){//1
					 if(exp.exp_gauche.with_not)//^1.p=0
						 return  this.get0Expression();
					 else//1.p=p
						 return  exp.exp_droite;
				 }
				 if(exp.exp_gauche.name_action.equals("0")){//0
					 if(exp.exp_gauche.with_not)//^0.p=p
						 return  exp.exp_droite;
					 else//0.p=0
						 return  this.get0Expression();
					 
				 } 
			
			//verification de droite	 
			if(exp.exp_droite.name_action.equals("1")){//1
					 if(exp.exp_droite.with_not)//p.^1=0
						 return  this.get0Expression();
					 else//p.1=p
						 return  exp.exp_gauche;
					 
			}	 
				 if(exp.exp_droite.name_action.equals("0")){//0
					 if(exp.exp_droite.with_not)//p.^0=p
						 return  exp.exp_gauche;
					 else//p.^0=0
						 return  this.get0Expression();
				 }  
		 } 
		
		 if(exp.operateur ==  EXPRESSION_SYMBOL.operator_union){
				//verification de gauche 
				 if(exp.exp_gauche.name_action.equals("1")){//1
					 if(exp.exp_gauche.with_not)//^1+p=p
						 return exp.exp_droite;
					 else//1+p=1
						 return  this.get1Expression();
				 }
				 if(exp.exp_gauche.name_action.equals("0")){//0
					 if(exp.exp_gauche.with_not) //^0+p=1
						 return  this.get1Expression();	  
					  else //0+p=p
						  return exp.exp_droite;
					  
				 } 
			 
			//verification de droite	 
			if(exp.exp_droite.name_action.equals("1")){//1
				 if(exp.exp_droite.with_not) //p+^1=p
					 return exp.exp_gauche;
				  else //p+1=1
					  return  this.get1Expression();	  
			}	 
				 if(exp.exp_droite.name_action.equals("0")){//0
					 if(exp.exp_droite.with_not) //p+^0=1
						 return  this.get1Expression();
					  else //p+^0=p
						  return exp.exp_gauche;  
				 }  
			 
		 }
 
		 return exp;
	 }
	 
	private Expression get1Expression(){
		Expression exp = new Expression();
		 exp.exp_droite = null;
		 exp.exp_gauche = null;
		 exp.name_action ="1";
		 exp.with_not =false;
		 exp.with_star =false;
		 return exp;
	}
	 
	private Expression get0Expression(){
		Expression exp = new Expression();
		exp.exp_droite = null;
		exp.exp_gauche = null;
		exp.name_action ="0";
		exp.with_not =false;
		exp.with_star =false;
		return exp;
	}
	
	public Expression clone() {
		Expression exp_col = null;
	    try { 
	    	exp_col = (Expression) super.clone();
	    } catch(CloneNotSupportedException cnse) { 
	      	cnse.printStackTrace(System.err);
	    }
 
	    return exp_col;
	}
}
 
