package com.derivatExpression;

import java.util.ArrayList; 
import java.util.List; 
 

public class ExpressionAdapter {
	
	public static void main(String[] argv){
		ExpressionAdapter e = new ExpressionAdapter("send+recive.send+(send.recive).send*recive");
		Expression ex = e.getExpression();

		System.out.println(ex.toString());
	}
	
	private List<SymbolAdapter>		symbolList = new ArrayList<SymbolAdapter>();

	
	public class SymbolAdapter {
		public String symbol = "";
		public Integer index = 0;
		
		public SymbolAdapter(Integer index,String symbol){
			this.index = index;
			this.symbol = symbol;
		}
		public SymbolAdapter(Integer index){
			this.index = index;
		}
		public String toString(){
			return "("+index+","+symbol+")";
		}
	}
	
	public class  EXPRESSION_SYMBOL
	{
		public static final int operator_union		    = 1;//+
		public static final int operator_concatenation  = 2;//.
		public static final int operator_nothing		= 3;// after the start expression
		public static final int alpha 					= 4;//Alphabetic
		public static final int binary_not 				= 5;//^
		public static final int binary_star 			= 6;//*
		public static final int parenthesis_open		= 7;//(
		public static final int parenthesis_close 		= -7;//)
		public static final int start_expression 		= 8;// Expression start
		public static final int end_expression 		    = -8;//fin d'expression
		
	}
	
	public class ExpressionSplite{
		public List<SymbolAdapter>	 leftExpression = new ArrayList<SymbolAdapter>();
		public List<SymbolAdapter>	 rightExpression = new ArrayList<SymbolAdapter>();;
		public SymbolAdapter         mainOperator ;
	}
 
	public Expression getExpression(){
		 
		Expression new_exp = new Expression();
		
		
		if(this.isAction(this.symbolList)){
		 
			new_exp.with_not  = this.iswith_not(this.symbolList);
			new_exp.with_star = this.iswith_start(this.symbolList);
			
			if(new_exp.with_not && new_exp.with_star) 
				new_exp.not_is_first = this.not_is_first(this.symbolList);
				 
			 
			new_exp.name_action =  this.symbolList.get(1).symbol;
			
		}else{
			 
			new_exp.with_not  = this.iswith_not(this.symbolList);
			new_exp.with_star = this.iswith_start(this.symbolList);
			
			if(new_exp.with_not && new_exp.with_star) 
				new_exp.not_is_first = this.not_is_first(this.symbolList); 
			 
			symbolList = this.delete_note(this.symbolList);		
			symbolList = this.delete_star(this.symbolList);
			symbolList = this.delete_note(this.symbolList);
			symbolList = this.delete_parentese(this.symbolList);
			
			ExpressionSplite splite = this.getMainOperatorIndex(this.symbolList);
			
			new_exp.operateur = splite.mainOperator.index;
			
			ExpressionAdapter new_left_expression = new ExpressionAdapter(splite.leftExpression);
			ExpressionAdapter new_right_expression = new ExpressionAdapter(splite.rightExpression);
			
			new_exp.exp_gauche  = new_left_expression.getExpression();
			new_exp.exp_droite = new_right_expression.getExpression();
			
		}
		
		return new_exp;
	}
 
	private ExpressionSplite getMainOperatorIndex(List<SymbolAdapter> symbolList) {
		
		//Pick the major main operator 
		//According the best operator, we divide the expression in tow parts. 
 
		List<Integer> operatorsList = new ArrayList<Integer>();
		
		symbolList = this.delete_parentese(symbolList);
		
		for(int i = 0; i<symbolList.size();i++){
			
			SymbolAdapter sa = symbolList.get(i);
			
			if(sa.index == EXPRESSION_SYMBOL.parenthesis_open){
				//skip to next close parentheses
				for(int j = i; 
						symbolList.get(i).index != EXPRESSION_SYMBOL.parenthesis_close
						&& j<symbolList.size() ;j++,i++);
			}
			
			if(sa.index == EXPRESSION_SYMBOL.operator_union 
					|| sa.index  == EXPRESSION_SYMBOL.operator_concatenation
					|| sa.index  == EXPRESSION_SYMBOL.operator_nothing){
				
				operatorsList.add(i);	
				
			}
		}
		
		
		int bestOperator = -1;
		
		
		//pick the major operator
		if(bestOperator == -1) 
			for(int position : operatorsList) 
				if(symbolList.get(position).index == EXPRESSION_SYMBOL.operator_concatenation){
					bestOperator = position;
					break;
				}
		
		for(int position : operatorsList)  
			if(symbolList.get(position).index == EXPRESSION_SYMBOL.operator_union  ){
				bestOperator = position;
				break;
			}
 
		if(bestOperator == -1) 
			for(int position : operatorsList) 
				if(symbolList.get(position).index == EXPRESSION_SYMBOL.operator_nothing){
					bestOperator = position;
					break;
				} 

		ExpressionSplite splite = new ExpressionSplite();
		splite.mainOperator = symbolList.get(bestOperator);
		 
		for(int i =0 ; i <bestOperator;i++){ 
			splite.leftExpression.add(symbolList.get(i)); 
			
		}
		
		splite.leftExpression.add(new SymbolAdapter(EXPRESSION_SYMBOL.end_expression));
		
		splite.rightExpression.add(new SymbolAdapter(EXPRESSION_SYMBOL.start_expression));	
		for(int i =bestOperator+1 ; i <symbolList.size();i++)
			splite.rightExpression.add(symbolList.get(i));
		
		return splite;
	}

	public ExpressionAdapter(String expr){
		this.symbolList = this.stringToArray(expr);
		
	}
	
	public ExpressionAdapter(List<SymbolAdapter> symbolList){
		this.symbolList = symbolList;
	}
 
	/*private String adapterToString(List<SymbolAdapter> list){
		String str = "";
		for (SymbolAdapter symbolAdapter : list) 
			str += symbolAdapter.symbol;
		return str;
	}*/
	
	private ArrayList<SymbolAdapter> stringToArray(String expressionString) {
	
		
		ArrayList<SymbolAdapter> symbolList = new ArrayList<SymbolAdapter>();
		
		int taille = expressionString.length();

		symbolList.add(new SymbolAdapter(EXPRESSION_SYMBOL.start_expression));
		
		for(int i=0	;	i < taille	;	i++){
			
			char char_exp = expressionString.charAt(i);
		
			if( this.isAlpha(char_exp) ) {
 
				String alpha = ""+char_exp;
				
				for(i++; i < taille ; i++){
					
					char_exp = expressionString.charAt(i);
					
					if( !this.isAlpha(char_exp))
						break;
					
					alpha += char_exp;
	
				} 
				
				symbolList.add(new SymbolAdapter(EXPRESSION_SYMBOL.alpha,alpha));
				
			}
			
			if(char_exp == '-')
				symbolList.add(new SymbolAdapter(EXPRESSION_SYMBOL.binary_not,String.valueOf(char_exp)));
			
			if(char_exp == '+')
				symbolList.add(new SymbolAdapter(EXPRESSION_SYMBOL.operator_union,String.valueOf(char_exp)));
			
			if(char_exp == '.')
				symbolList.add(new SymbolAdapter(EXPRESSION_SYMBOL.operator_concatenation,String.valueOf(char_exp)));
		
			if(char_exp == '*'){
				symbolList.add(new SymbolAdapter(EXPRESSION_SYMBOL.binary_star,String.valueOf(char_exp)));
				symbolList.add(new SymbolAdapter(EXPRESSION_SYMBOL.operator_nothing));
				
			}
			if(char_exp == '(')
				symbolList.add(new SymbolAdapter(EXPRESSION_SYMBOL.parenthesis_open,String.valueOf(char_exp)));
			
			if(char_exp == ')')
				symbolList.add(new SymbolAdapter(EXPRESSION_SYMBOL.parenthesis_close,String.valueOf(char_exp)));  		  
		}
		
		symbolList.add(new SymbolAdapter(EXPRESSION_SYMBOL.end_expression));
		
		return symbolList;
	}
	
	private boolean isAlpha(char c){
		return Character.isLetter(c) || Character.isDigit(c);
	}
	
	/**
	 * this function check the expression is action or not
	 * @param index_list
	 * @return
	 */
	private boolean isAction(List<SymbolAdapter> index_list){
		
		index_list = this.delete_parentese(symbolList);
		
		if(index_list.size() == 2) {
			System.out.print("Expression is empty so action return false");
			return false;
		}
		
		int[] succ_debut 				= {EXPRESSION_SYMBOL.parenthesis_open,EXPRESSION_SYMBOL.alpha,EXPRESSION_SYMBOL.binary_not};
		int[] succ_alpha 				= {EXPRESSION_SYMBOL.parenthesis_close,EXPRESSION_SYMBOL.alpha,EXPRESSION_SYMBOL.end_expression,EXPRESSION_SYMBOL.binary_star};
		int[] succ_not 					= {EXPRESSION_SYMBOL.alpha,EXPRESSION_SYMBOL.parenthesis_open};
		int[] succ_star					= {EXPRESSION_SYMBOL.parenthesis_close,EXPRESSION_SYMBOL.end_expression };
		int[] succ_parentese_ouvrant 	= {EXPRESSION_SYMBOL.binary_not,EXPRESSION_SYMBOL.alpha};
		int[] succ_parentese_ferment 	= {EXPRESSION_SYMBOL.end_expression,EXPRESSION_SYMBOL.binary_star};

		for(int i=0 ; i<index_list.size();i++){
			Integer integer = index_list.get(i).index;
			
			switch(integer){
			  case EXPRESSION_SYMBOL.start_expression :
				  integer = index_list.get(i+1).index;
				 if( this.verif_secc(succ_debut,integer) == false ){
					 return false;
				 }
					
				  break;
			  case EXPRESSION_SYMBOL.alpha :
				  integer = index_list.get(i+1).index;
				  if( this.verif_secc(succ_alpha,integer) == false ) {
						
						 return false;
					 }
				  break;
			  case EXPRESSION_SYMBOL.binary_not :
				  integer = index_list.get(i+1).index;
				   if( this.verif_secc(succ_not,integer) == false ) {
						
						 return false;
					 }
				break;
			  case EXPRESSION_SYMBOL.binary_star :
				  integer = index_list.get(i+1).index;
					 if( this.verif_secc(succ_star,integer) == false ){
						 return false;
					 }
					  break;
			  case EXPRESSION_SYMBOL.parenthesis_open :
				  integer = index_list.get(i+1).index;
				  if( this.verif_secc(succ_parentese_ouvrant,integer) == false ) {
						 return false;
					 }
				  break;
			  case EXPRESSION_SYMBOL.parenthesis_close :
				  integer = index_list.get(i+1).index;
				  if( this.verif_secc(succ_parentese_ferment,integer) == false ) {
						 return false;
					 }
				  break;
			  case EXPRESSION_SYMBOL.end_expression : 
				  return true;
				  
			}
			
		}
 
		return true;
	}

	private boolean verif_secc(int[] succ_debut, Integer integer) {

		for (int i : succ_debut) {
			if(i == integer) return true;
		}
		
		return false;
	}

	private boolean iswith_start(List<SymbolAdapter> index_list){
		 
		index_list = this.delete_parentese(index_list);
	
		int nb_element = index_list.size();
		
		Integer symbole = index_list.get(nb_element-2).index;
		
		if(symbole != EXPRESSION_SYMBOL.binary_star && symbole != EXPRESSION_SYMBOL.parenthesis_close)
				return false;
		
		if(symbole == EXPRESSION_SYMBOL.parenthesis_close){
			symbole = index_list.get(1).index;
			
			if(symbole != EXPRESSION_SYMBOL.binary_not)
				return false;
			
			symbole = index_list.get(nb_element-4).index;
		}else{
		 symbole = index_list.get(nb_element-3).index;
		}
		
	 switch(symbole){
		 case EXPRESSION_SYMBOL.alpha :
			 
			 for(int i = nb_element-4;i>=0;i--){
				 symbole = index_list.get(i).index;
				  if(symbole != EXPRESSION_SYMBOL.alpha && 
					 symbole != EXPRESSION_SYMBOL.binary_not &&
					 symbole != EXPRESSION_SYMBOL.parenthesis_open &&
					 symbole != EXPRESSION_SYMBOL.start_expression)
					  return false;
				  
			  }
			  return true;
			 
		 case EXPRESSION_SYMBOL.parenthesis_close :
			 
			 int nb_parentese_ferment = 1;
			 
				
				for(int i = nb_element-4;i>=0;i--){
					symbole = index_list.get(i).index;
					
					  if(symbole == EXPRESSION_SYMBOL.parenthesis_close) 
						  nb_parentese_ferment++;
					 
					  if(symbole == EXPRESSION_SYMBOL.parenthesis_open){
						  if(nb_parentese_ferment==1){
							  symbole = index_list.get(i-1).index;
							  
							  if(symbole == EXPRESSION_SYMBOL.binary_not){
								  symbole = index_list.get(i-2).index;
								  
								  if(symbole == EXPRESSION_SYMBOL.start_expression){
									  return true;
								  }
								  
								  return false;
								  
							  }else if(symbole == EXPRESSION_SYMBOL.start_expression){
								  return true;
							  }
							  
							 return false;
						  }
						  nb_parentese_ferment--;
					  }
					 
				  }
			  return false;	
		}
		return false;
	}
	
	private boolean iswith_not(List<SymbolAdapter> index_list){
 
		index_list = this.delete_parentese(index_list);
		
		int nb_element = index_list.size();
		
		Integer symbole = index_list.get(1).index;
	 
		if(symbole != EXPRESSION_SYMBOL.binary_not && symbole != EXPRESSION_SYMBOL.parenthesis_open)
			return false;
	
			if(symbole == EXPRESSION_SYMBOL.parenthesis_open){
				symbole = index_list.get(nb_element-2).index;
				
				if(symbole != EXPRESSION_SYMBOL.binary_star)
					return false;
				
				symbole = index_list.get(3).index;
			}else{
			 symbole = index_list.get(2).index;
			}
 
		switch(symbole){
			case EXPRESSION_SYMBOL.alpha :
				 for(int i = 3;i<nb_element;i++){
					 symbole = index_list.get(i).index;
					  if(symbole != EXPRESSION_SYMBOL.alpha && 
						 symbole != EXPRESSION_SYMBOL.binary_star &&
						 symbole != EXPRESSION_SYMBOL.parenthesis_close &&
						 symbole != EXPRESSION_SYMBOL.end_expression)
						  return false;
				  }
				  return true;
			case EXPRESSION_SYMBOL.parenthesis_open :
				
				List<Integer> parentese_ouvert = new ArrayList<Integer>();
				parentese_ouvert.add(2);
				
				for(int i = 3;i<nb_element;i++){
					symbole = index_list.get(i).index;
					
					  if(symbole == EXPRESSION_SYMBOL.parenthesis_open) 
						  parentese_ouvert.add(i);
					 
					  if(symbole == EXPRESSION_SYMBOL.parenthesis_close){
						  if(parentese_ouvert.size()==1){
							  symbole = index_list.get(i+1).index;
							  
							  if(symbole == EXPRESSION_SYMBOL.binary_star){
								  symbole = index_list.get(i+2).index;
								  
								  if(symbole == EXPRESSION_SYMBOL.end_expression){
									  return true;
								  }
								  
								  return false;
								  
							  }else if(symbole == EXPRESSION_SYMBOL.end_expression){
								  return true;
							  }
							  
							 return false;
						  }
						  parentese_ouvert.remove(parentese_ouvert.size()-1);
					  }
					 
				  }
			  return false;		
		}
 
		return false;
	}
	
	private List<SymbolAdapter> delete_parentese(List<SymbolAdapter> index_list){
 
 
		Integer symbole = index_list.get(1).index;
		
		if(symbole != EXPRESSION_SYMBOL.parenthesis_open   ) 
			return index_list;
			
		int nb_parentese_droit = 0;
		
		for(int i=2 ; i<index_list.size();i++){
			
			symbole = index_list.get(i).index;
			
			if(symbole == EXPRESSION_SYMBOL.parenthesis_open   ) 
				nb_parentese_droit++;
			 
		 	
			if(symbole == EXPRESSION_SYMBOL.parenthesis_close   ){
				nb_parentese_droit--;
				
				symbole = index_list.get(i+1).index;
				
				if(symbole != EXPRESSION_SYMBOL.end_expression && nb_parentese_droit == -1)
						return index_list;
				
				if(symbole == EXPRESSION_SYMBOL.end_expression && nb_parentese_droit == -1){	
					index_list.remove(1);
					index_list.remove(index_list.size()-2);
					
					return delete_parentese(index_list);
				}
 
			}
		}
 
		return  index_list;
		
	}

	private List<SymbolAdapter> delete_note(List<SymbolAdapter> index_list){
		
		index_list = this.delete_parentese(index_list); 
		
		if(! this.iswith_not(index_list))
			return index_list;
 
		Integer symbole = index_list.get(1).index;
		
		if(symbole != EXPRESSION_SYMBOL.binary_not  ) 
			return index_list;
		
		index_list.remove(1);
 
		return this.delete_note(index_list);
	}
	
	private List<SymbolAdapter> delete_star(List<SymbolAdapter> index_list){
		
		index_list = this.delete_parentese(index_list); 
		
		if( ! this.iswith_start(index_list))
			return index_list;
 
		Integer symbole = index_list.get( index_list.size()-2).index;
		
		if(symbole != EXPRESSION_SYMBOL.binary_star   ) 
			return index_list;
		
		index_list.remove( index_list.size()-2);
		
		return this.delete_star(index_list);
	}
	
	private boolean not_is_first(List<SymbolAdapter> index_list) {
		index_list = delete_parentese(index_list);
		return index_list.get(1).index == EXPRESSION_SYMBOL.binary_not;
	}

	public static String symbolToOperateur(int symbole){
		 
		if( symbole==EXPRESSION_SYMBOL.operator_concatenation)
			return ".";
		if( symbole==EXPRESSION_SYMBOL.operator_union)
			return "+";
		if( symbole==EXPRESSION_SYMBOL.operator_nothing)
			return "";
	return "";
}

}
