package com;
  
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue; 
import java.awt.Graphics2D;

import javax.imageio.ImageIO;
import javax.swing.JFrame; 
import javax.swing.JTextField;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.swing.JFileChooser; 
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel; 
import javax.swing.ScrollPaneConstants;

import com.aldebaran.Graph;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent; 
import java.awt.image.BufferedImage;

import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants; 
 
public class MainWindowSwing {

	private JFrame frmDerivativeOfA;
	private JTextField txtFileAldebaran; 
	private JTextField textPathExpression;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MainWindowSwing window = new MainWindowSwing();
					window.frmDerivativeOfA.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindowSwing() {
		initialize();
	}

 
	private void initialize() {
		frmDerivativeOfA = new JFrame();
		frmDerivativeOfA.setTitle("Derivative of a Labeled Transition  System");
		frmDerivativeOfA.setBounds(100, 100, 585, 237);
		frmDerivativeOfA.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDerivativeOfA.getContentPane().setLayout(null);
		
		textPathExpression = new JTextField();
		textPathExpression.setBounds(10, 124, 355, 20);
		frmDerivativeOfA.getContentPane().add(textPathExpression);
		textPathExpression.setColumns(10);
		textPathExpression.setText("/home/mohamed/Desktop/PasswordProperties.txt");
		
		JLabel lblExpression = new JLabel("Expression file path ");
		lblExpression.setBounds(10, 106, 159, 14);
		frmDerivativeOfA.getContentPane().add(lblExpression);
		
		JButton btnNewButton_1 = new JButton("View");
		btnNewButton_1.setBounds(454, 165, 105, 23);
		frmDerivativeOfA.getContentPane().add(btnNewButton_1);
		
		JLabel lblCheminFichier = new JLabel("Aldebaran file path");
		lblCheminFichier.setBounds(10, 50, 159, 14);
		frmDerivativeOfA.getContentPane().add(lblCheminFichier);
		
		txtFileAldebaran = new JTextField();
		txtFileAldebaran.setHorizontalAlignment(SwingConstants.LEFT);
		txtFileAldebaran.setBounds(10, 75, 355, 20);
		frmDerivativeOfA.getContentPane().add(txtFileAldebaran);
		txtFileAldebaran.setText("/home/mohamed/Desktop/Password.aut");
		txtFileAldebaran.setColumns(40);
		
		JButton btnSelectAldebaran = new JButton("Select");
		btnSelectAldebaran.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectFileAldebarane();
			}
		});
		btnSelectAldebaran.setBounds(374, 74, 111, 23);
		frmDerivativeOfA.getContentPane().add(btnSelectAldebaran);
		
		JButton btnSelectExpression = new JButton("Select");
		btnSelectExpression.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectFileExpression();
			}
		});
		btnSelectExpression.setBounds(374, 123, 111, 23);
		frmDerivativeOfA.getContentPane().add(btnSelectExpression);
	 
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				viewGraphe();
			}
		});
		
		  	
  
	}
	JPanel panel_1 ;
	public void selectFileAldebarane(){
		final JFileChooser fc = new JFileChooser();
	 
		 int returnVal = fc.showOpenDialog(frmDerivativeOfA);

        if (returnVal == JFileChooser.APPROVE_OPTION)  {
            File file = fc.getSelectedFile();
            
            txtFileAldebaran.setText(file.getAbsolutePath());
        }   
        
  
	}
	public void selectFileExpression(){
		final JFileChooser fc = new JFileChooser();
		
		int returnVal = fc.showOpenDialog(frmDerivativeOfA);
		
		if (returnVal == JFileChooser.APPROVE_OPTION)  {
			File file = fc.getSelectedFile();
			
			textPathExpression.setText(file.getAbsolutePath());
		}   
		
		
		
	}
	public void viewGraphe(){
 
		  final  File fileAdebaran = new File(txtFileAldebaran.getText()); 
		  final  File fileExpression = new File(textPathExpression.getText()); 
		 
			
        final Graph  graphe = new Graph(fileAdebaran,fileExpression);
          int width = graphe.getWidthGraph();
         
        graphe.setPreferredSize(new Dimension(width+200,200));
 
        JFrame frame = new JFrame("DrawGraph");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
    	JButton btnNewButton_2 = new JButton("Save as image");
		btnNewButton_2.setBounds(10,10,200,25);
        frame.getContentPane().add(btnNewButton_2);
        
        btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BufferedImage  bi = new BufferedImage(graphe.getWidth(),graphe.getHeight(), BufferedImage.TYPE_INT_ARGB); 
			    Graphics2D gd =  bi.createGraphics();
			    gd.setBackground(Color.WHITE);
			    graphe.paint(gd);
			   
			    try {
			    	
					ImageIO.write(bi, "gif", new File("/home/mohamed/Desktop/"+new Date().getTime()+".gif"));
				} catch (IOException e1) {
					 
					e1.printStackTrace();
				}
			}
		});
        
        
        JScrollPane scrollPane = new JScrollPane(graphe);  
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);  
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);  
        
        
        frame.getContentPane().add(scrollPane);  
 
        frame.setSize(800, 600); 
        frame.setVisible(true); 
 
	}
}
