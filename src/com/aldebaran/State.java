package com.aldebaran;
 
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class State {

	private String Name ;
	private Boolean isInitial =false;
	private Color color = Color.GREEN;
	private Point point;
	
	private List<Transition> transitionIn = new ArrayList<Transition>();
	private List<Transition> transitionOut = new ArrayList<Transition>();
	
	 public void addTransitionIn(Transition newTransition){
		 transitionIn.add(newTransition);
	 }
	 public void addTransitionOut(Transition newTransition){
		 transitionOut.add(newTransition);
	 }
	public List<Transition> getTransitionIn() {
		return transitionIn;
	}
	public void setTransitionIn(List<Transition> transitionIn) {
		this.transitionIn = transitionIn;
	}
	public List<Transition> getTransitionOut() {
		return transitionOut;
	}
	public void setTransitionOut(List<Transition> transitionOut) {
		this.transitionOut = transitionOut;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public Boolean getIsInitial() {
		return isInitial;
	}
	public void setIsInitial(Boolean isInitial) {
		this.isInitial = isInitial;
	}
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public Point getPoint() {
		return point;
	}
	public void setPoint(Point point) {
		this.point = point;
	}
	
	
	
	
}
