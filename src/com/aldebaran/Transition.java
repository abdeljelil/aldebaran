package com.aldebaran;

import java.awt.Color;
 
public class Transition {

	private String name = "";
	private String desination ="";
	private Color color = Color.BLACK;
	private State statInitial ;
	private State statFinal;
	
 
	
	public String getDesination() {
		return desination;
	}
	public void setDesination(String desination) {
		this.desination = desination;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public State getStateInitial() {
		return statInitial;
	}
	public void setStateInitial(State statInitial) {
		this.statInitial = statInitial;
	}
	public State getStateFinal() {
		return statFinal;
	}
	public void setStateFinal(State statFinal) {
		this.statFinal = statFinal;
	}
	
	
	
}
