package com.aldebaran;
 
import java.awt.Color;
import java.awt.Font;
  
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D; 
import java.awt.Point;

import javax.swing.JPanel;

public class GraphDraw   extends JPanel{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int SPACE_Y = 250;
	public static int SPACE_X = 120;
	public static int POSITION_INIT_X = 80;
	public static int POSITION_INIT_Y = 150;
	public static int STAT_HIEGHT = 50;
	public static int STAT_WIDTH = 50;
 
	
	public  void drawState(Graphics2D g,Point position,Color color,String text){
  
		  
		 g.setColor(color);
		 
		 Point p1 = new Point(position.x-(STAT_WIDTH/2),position.y-(STAT_HIEGHT/2));
		  
		 
		 Ellipse2D.Double ellipse  = new Ellipse2D.Double(p1.x, p1.y,STAT_WIDTH, STAT_HIEGHT);
		 g.fill(ellipse);
		 
		 //contoure
		 g.setColor(Color.BLACK);
         ellipse  = new Ellipse2D.Double(p1.x, p1.y,STAT_WIDTH, STAT_HIEGHT);
		 g.draw(ellipse);
		 
		//lib de etat
		 int taille_text = text.length() ;
		 Point pText = new Point(position.x, position.y);
		 pText.y = position.y;
		 pText.x = position.x-taille_text;
			
		 Font font = new Font("Courier", Font.BOLD, 14);
		 g.setFont(font);
		 g.setColor(Color.BLACK);          
		 g.drawString( text,pText.x , pText.y);
		 
		 
		
	}
	public void drawTransition(Graphics2D gr,Point p1,Point p2,int niveau,Color color,String text){
		
		//TODO
		Point p1REf = new Point(p1.x+25,p1.y);
		Point p2REf = new Point(p2.x-32,p2.y);
  
		new CurvedArrow().draw( gr, p1REf, p2REf, text,color);
 
	}

 

 

}
