package com.aldebaran;
 
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon; 
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;


public class CurvedArrow  
{
	
	 
    // to draw a nice curved arrow, fill a V shape rather than stroking it with lines
	public  void draw ( Graphics2D g  , Point p1,Point p2,String text,Color color )
    {
		 
		AffineTransform transformInit = g.getTransform();
		g.setColor(color);
        Line2D.Double line =  new Line2D.Double ( p1.x,p1.y, p2.x,p2.y);
        g.draw (line );
         
		double ongle = this.getAngle(p1, p2);
	 
		Point ref = this.getInitPoint( p1, p2);
		
		AffineTransform transform = new AffineTransform();
		
		transform.translate (p2.x,p2.y);
		transform.rotate(ongle);
           
		g.setTransform(transform);

   		 int[] xs = { 0, -15, -15};
   		 int[] ys = { 0, -10, 10 };
   		 
			if(   !ref.equals(p2) ){
				xs[0]=-10;	ys[0]=0;	
				xs[1]=0;	ys[1]=-10;	
				xs[2]=0;	ys[2]=10;	
			    		}
			
   		 Polygon triangle = new Polygon(xs, ys, xs.length);
   		 g.fillPolygon(triangle);

   		this.drowText(g,p1,p2,text);
 
		 g.setTransform(transformInit);
    }
	
    private void drowText(Graphics2D g,  Point p1,Point p2,String text){
    	

    	AffineTransform transform2 = new AffineTransform();
    	 
    	int xTxt = (Math.abs(p1.x-p2.x)/2)+Math.min(p1.x, p2.x);
    	int yTxt = (Math.abs(p1.y-p2.y)/2)+Math.min(p1.y, p2.y);
    	   
    	 
    	 
 
    	g.setTransform(transform2);
   		 
		 Font font = new Font("Courier", Font.BOLD, 12);
		 g.setFont(font);
		 g.setColor(Color.BLACK);      
		 g.setBackground(Color.WHITE);
		 g.drawString(text,xTxt , yTxt);
 
    	
    }
    private double getAngle(Point p1,Point p2){
    	
    	 double L =  p1.x-p2.x ;
    	 double H =  p1.y-p2.y; 
    	 
    	 double a =  Math.pow(L, 2)+Math.pow(H, 2);
   		
    	 a = Math.sqrt( a ); // hypothèse
   		
   		double theta = 0;
   		
   		if(L==0) {
   			  theta = 90;
   		}else if (H==0){
   			  theta = 0;
   		}else 
   			   theta =Math.atan( H / L)+Math.toRadians(180);
   			
   		
		return theta;
	}
    private Point getInitPoint(Point p1,Point p2){
    	
    	if(p1.x==p2.x && p1.y > p2.y )return p2;
    	if(p1.x==p2.x && p1.y < p2.y )return p1;
    	
    	if(p1.y==p2.y && p1.x > p2.x) return p1;
    	if(p1.y==p2.y && p1.x < p2.x) return p2;
    	  
    	
    	if(p1.x > p2.x && p1.y > p2.y) return p2;  	
    	if(p1.x < p2.x && p1.y < p2.y) return p1;
    	
    	if(p1.x > p2.x && p1.y < p2.y) return p2;
    	
    	if(p1.x < p2.x && p1.y > p2.y) return p1;
 
    	return p1;
    }

}