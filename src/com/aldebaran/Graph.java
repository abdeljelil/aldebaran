package com.aldebaran;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.awt.Point;

import com.derivatExpression.Derivative;
import com.derivatExpression.Expression; 
import com.derivatExpression.ExpressionAdapter;

public class Graph  extends GraphDraw{
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Transition> liste_transition = new ArrayList<Transition>();
	 
	 Expression  expressionGlobal =null;
	
	public Graph(File fileAldebaran,File fileexpression){
	
		 this.getFileTolistExpression(fileexpression);
		
		this.chargeTransitionList(fileAldebaran);
	}
	
	private void getFileTolistExpression(File fileexpression) {
		 
		expressionGlobal = new  Expression();
		
		List<String> contnu_Exp_file = this.fileToString(fileexpression);
		
		if(contnu_Exp_file == null || contnu_Exp_file.size()==0) return  ;
 
		  String string  = contnu_Exp_file.get(0);
		  string=  string.replace(" ", "");
		  expressionGlobal = new ExpressionAdapter(string).getExpression();
		 
 
	}



	private  void  chargeTransitionList(File  fileAldebaran){
		List<String> contnu_aut_file = this.fileToString(fileAldebaran);
		
		if(contnu_aut_file == null || contnu_aut_file.size()==0) return  ;
 
	  String etat_inital = ""; 
 
		 for (String string : contnu_aut_file) {
 
				 string=  string.replace(" ", "");
				 
				 
				  if(string!= null && string.length() > 6 ){
					  
					  if(string.contains("des")){
						  int endIndex = string.indexOf(",");
						  etat_inital = string.substring(4, endIndex); 
						  System.out.println("INIT : "+string);
					  }else{
						
						  String etat_init 			= this.getElementPostion(string,1);
						  String etat_final  		= this.getElementPostion(string,3);
						  String etat_transition  	= this.getElementPostion(string,2);
						 
						  Transition t = new Transition( );
						  t.setDesination(etat_transition);
						  t.setName(etat_init+","+etat_transition+","+etat_final);
						  State State_init  = this.getStateByName(etat_init);
						  State State_final ;
						  
						  if(etat_init.equals(etat_final))
							  State_final = State_init;
						  else
							  State_final = this.getStateByName(etat_final);
						  
						  t.setStateFinal(State_final);
						  t.setStateInitial(State_init); 
						  
						  liste_transition.add(t);
						
					  }
			    	} 
			
			 }
	  

		 State s= this.getStateByName(etat_inital);
	  if(s != null)  
		  s.setIsInitial( true); 
	   
	  this.chargeListTransitionsInStates();
	  
	  this.reglagePostionState();
	  
	  this.monitorLTS();
 
	}
	private void monitorLTS() { 
		System.out.println(expressionGlobal.toString());
		
		State intState = this.getInitialState();
		 
		computeDerivatives(intState,expressionGlobal.clone());
	}
	private void  computeDerivatives(State state,Expression expression){
		 
		
		Boolean isMonitorable=false;
		List<Transition> A = state.getTransitionOut();
		for(Transition a : A)
		{
		//#calculate derivative of  with respect to a
 
			Expression exp_action = new  Expression();
			exp_action.name_action = a.getDesination();
 
			
			Derivative derivative = new Derivative(expression.clone(), exp_action);
			
			Expression phi_prime  = derivative.getExpression().purgeExpression();
			for (String string :  derivative.logDerv)  
				System.out.println(string);
		 
		if (expression.equals(phi_prime)==false) {
			a.setColor(Color.RED);//transition rouge
			
			if(isMonitorable==false){
			    //add to list
				state.setColor(Color.ORANGE); 
			    isMonitorable=true;
			}
		}
		
		//récuperation d'etat final de transition a
		State s_prime = a.getStateFinal();
		 
		computeDerivatives(s_prime,phi_prime);
		
		
		if(isMonitorable==false)
			a.setColor(Color.GREEN);
	
			
			System.out.println("("+state.getName() +","
					+a.getDesination()+","+s_prime.getName()+") "
					+ "Expression :"+expression.toString()+"@"+expression.hashCode()+",Derviée :"
					+phi_prime.toString()+",Color:"+a.getColor());
		}

		
	}
	private void chargeListTransitionsInStates(){
		List<State> listeStates = this.getListStates();
		for (State State : listeStates) {
			for (Transition transition : liste_transition) {
				
				if(transition.getStateFinal().equals(State))
					State.addTransitionIn(transition);
				else if(transition.getStateInitial().equals(State))
					State.addTransitionOut(transition);
				
			}
		}
	}
	 

	private State getStateByName(String State_name) {
		
		for (Transition t : liste_transition) {
			 
			if(t.getStateFinal().getName().equals(State_name))   return t.getStateFinal();
			if(t.getStateInitial().getName().equals(State_name)) return t.getStateInitial();
 
		}
		
		State s = new State();
		s.setName(State_name);
		 
		return s;
	}

	private String getElementPostion(String  string,int position){
		
		string = string.replace("(", "");
		string = string.replace(")", "");
		String[] stringTab = string.split(",");
		
		return stringTab[position-1];
	}
	
	private  List<String> fileToString(File file) {
		 BufferedReader buff = null;
		 List<String> list = new ArrayList<String>();
		 try {
 
			  buff = new BufferedReader(new FileReader(file));
			 String string;
			  
				 while ((string = buff.readLine()) != null) {
					 list.add(string);
				 }
				return list;  
		 } catch (IOException ioe) {
		  
			 System.out.println("Erreur --" + ioe.toString());
			 return list; 
		   }finally {
				  try {
					buff.close();
				} catch (IOException e) {
					 
					e.printStackTrace();
				}
			 }
	    }
	
	@Override
	public String toString(){
		String result = "";
		for (Transition t : liste_transition) {
			
			State s1 = t.getStateInitial();
			State s2 = t.getStateFinal();
			String part = "("+s1.getName()+","+t.getName()+","+s2.getName()+")";
			
			if(result.equals("")) 
				result +=part;
			 else 
				 result +="\n"+part;
 
		}
		String des = "";
		for (Transition t : liste_transition) {
			State s1 = t.getStateInitial();
			State s2 = t.getStateFinal();
			if(s1.getIsInitial()) 
				 des = "des("+s1.getName()+","+this.getNbEtat()+","+liste_transition.size()+")";
		 
			if(s2.getIsInitial()) 
				des = "des("+s2.getName()+","+this.getNbEtat()+","+liste_transition.size()+")";
		 
		}
		result  =des+"\n"+result;
		return result;
	}
	private List<State> getListStates(){
		List<State> list_State = new ArrayList<State>();
		 
		for (Transition t : liste_transition) {
			boolean exist = false;
			State s1 = t.getStateInitial();
			
			
			for (State s_re : list_State) {
				if(s_re.getName().equals(s1.getName()))
						exist = true;
			}
			
			if(exist==false) 
				list_State.add(s1);
			 
			 exist = false;
			 State s2 = t.getStateFinal();
			
			for (State s_re : list_State) {
				if(s_re.getName().equals(s2.getName()))
						exist = true;
			}
			
			if(exist==false) 
				list_State.add(s2);
		}
	 
		
		return list_State;
	}
	private int getNbEtat() {
		List<State> list_State = this.getListStates();
		
		return list_State.size();
	}

	private List<List<Transition>> getFamilleList(){
		
		 List<List<Transition>> listeFamille = new ArrayList<List<Transition>>();
 
		 for (Transition t1 : liste_transition) {
			
			 if(! this.isExisteInFamille(listeFamille,t1)){
				// System.out.println("t1 : "+ t1.getName());
				 List<Transition> item = new ArrayList<Transition>();
				 item.add(t1);
				 State s1t1 = t1.getStateInitial();
				 State s2t1 = t1.getStateFinal();
				 
				 for (Transition t2 : liste_transition) {
					 if( ! t1.getName().equals(t2.getName()) ){
						// System.out.println("t2 : "+ t2.getName());
						 State s1t2 = t2.getStateInitial();
						 State s2t2 = t2.getStateFinal();
						
						 if( ( s1t1.getName().equals(s1t2.getName()) && s2t1.getName().equals(s2t2.getName()) ) ||
						     ( s1t1.getName().equals(s2t2.getName()) && s2t1.getName().equals(s1t2.getName()) )){
							 item.add(t2);
						 }
					 } 
				 } 
				 
				 listeFamille.add(item);
			 }
		 }
		 
		
		 
		 return listeFamille;
	}

	private boolean isExisteInFamille(List<List<Transition>> listeFamille, Transition t1) {
	 
		for (List<Transition> list : listeFamille) {
			for (Transition transition : list) {
				if(t1.getName().equals(transition.getName())) return true;
			}
		}
		
		return false;
	}
	
	 @Override
    protected void paintComponent(Graphics g) {
		 
		 g.clearRect(0, 0, getWidth(), getHeight() );
		 
         this.draw((Graphics2D)g);
    }
	private void draw( Graphics2D g ){
		
		g.setRenderingHint ( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        g.setStroke ( new BasicStroke ( 1.5f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND ) );
 
		 List<State> liste = this.getListStates(); 
		 for (State state : liste) {
			 super.drawState(g,state.getPoint(),state.getColor(),state.getName());
		 }
		 
		List<List<Transition>> familleT = this.getFamilleList();
		for (List<Transition> list : familleT) {
			for(int i = 0 ; i <list.size();i++){
				Transition t = list.get(i);
				super.drawTransition(g, t.getStateInitial().getPoint(), t.getStateFinal().getPoint(),i, t.getColor(), t.getDesination());
			}
		}
	}
	
	public void logPositionState(){
		List<State> liste = getListStates(); 
		for (State state : liste) {
			System.out.println(state.getName() + " ("+state.getPoint().toString()+")");
		}
	}
	
	private void reglagePostionState(){
		 
		//calcule de la position Y
		State init_State = this.getInitialState();
		
		init_State.setPoint(new Point(0, POSITION_INIT_Y));
		this.reglagePostionState_Y(init_State);
		 
		//Calcule de la position X
		int width = this.getWidthGraph();
		List<State> liste = getListStates(); 
		for (State state : liste) {
			if(state.getPoint()!= null)
				if(state.getPoint().x == 0)
						this.reglagePostionState_X(state,width);
		}
 
		
		for (State state : liste) {
			Point p =  state.getPoint();
			int tmp = p.x;
			p.x = p.y;
			p.y= tmp;
			state.setPoint(p);
		}

	}
	public int getWidthGraph(){
		int width = 0;
 
		List<State> liste = getListStates(); 
		for (State state : liste) {
			 
			List<State> liste_Relation = this.getStatesWithRelation_X(state);
			int width_tmp = (liste_Relation.size()-1) * SPACE_X + (POSITION_INIT_X*2);
			if(width_tmp >width ) width = width_tmp;
		}
		return width;
	}
	private void reglagePostionState_X(State State_ser,int width){
		
		List<State> liste_Relation = this.getStatesWithRelation_X(State_ser);
	 
		int taille_compos = (liste_Relation.size()-1) * SPACE_X;
		
		int position_X = width/2 - taille_compos/2;
		
		for (State state : liste_Relation) {
			Point p = state.getPoint();
			p.x = position_X;
			state.setPoint(p);
			position_X += SPACE_X;
		}
		
		
	}
	private void reglagePostionState_Y(State state_ser){
		
		List<State> state_relation = this.getStatesWithRelation_Y(state_ser);
		 
		for (State state : state_relation) {
			
			state.setPoint(new Point(0, state_ser.getPoint().y+SPACE_Y));
		}
		
		for (State State : state_relation) {
			this.reglagePostionState_Y(State);
		}
		
		
	}
	
	private State getInitialState(){
		List<State> liste_State = this.getListStates();
		 
		for (State state : liste_State) {
		   
			if(state.getIsInitial() == true)  {
			 
				return state;
			}
		}
		return null;
	}
	
	private List<State> getStatesWithRelation_Y(State State_ser){
		
		List<State> liste_State = new ArrayList<State>();
		
		for (Transition transition : liste_transition) {
			
			State s1 = transition.getStateInitial();
			State s2 = transition.getStateFinal();
			State relation = isRelation(State_ser,s1,s2);
			if(relation != null){
				boolean trouve = false;
				for (State State : liste_State) {
					if(State.getName().equals(relation.getName())) 
						trouve = true;			 
				}
				if(!trouve){
					if(relation.getPoint() != null ){
						if(relation.getPoint().y == 0 ) 
							liste_State.add(relation);
					}else{
						liste_State.add(relation);
					}
				}
				
			}
 
		}
		
		return liste_State;
	}

	private List<State> getStatesWithRelation_X(State state_ser){
		List<State> result = new ArrayList<State>();
		
		List<State> liste = this.getListStates();
		for (State state : liste) {
			if(state.getPoint() != null)
			if(state.getPoint().y == state_ser.getPoint().y)
				result.add(state);
		}
		
		return result;
	}
	
	private State isRelation(State state_ser,State s1,State s2){
		if(state_ser.getName().equals(s1.getName()))
			return s2;
		if(state_ser.getName().equals(s2.getName()))
			return s1;
		return null;
	}
}
